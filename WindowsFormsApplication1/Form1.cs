﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Web;
using System.Net.Mail;
using HtmlAgilityPack;
using System.Drawing;
using ScrapySharp.Extensions;






namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string tekst = textBox2.Text;
            string mail = textBox3.Text;
            string url = textBox1.Text;
            if (mail == "" |url == "" |tekst == "")
            {
                MessageBox.Show("Zima - brakuje maila, urla lub tekstu!");
                return;
            }
          
            String imageSRC = null;
            WebClient client = new WebClient();
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

            // Wczytywanie zawartości strony do stringa
            String html = client.DownloadString(url);
            System.IO.File.WriteAllText(@"D:\baba.txt", html); // sprawdzam "manualnie" czy pobral zrodlo
            // Ładowanie jako html
            doc.LoadHtml(html);
            // Pobranie wszystkich znaczników "<img ...."
            IEnumerable<HtmlNode> allImg = doc.DocumentNode.CssSelect("img").AsEnumerable();
    
            //Sprawdzamy kolejno
            foreach (HtmlNode n in allImg)
            {
                // Jeśli słowo  kluczowe wczytujemy obraz
                if (n.GetAttributeValue("alt").ToLower().Contains(tekst.ToLower()))
                {
                    loadImg(n.GetAttributeValue("src"));
                    
                    // wysylanie maila
                    MailMessage message = new MailMessage();
                    message.From = new MailAddress("meganfox@gmail.com");
                    message.To.Add(new MailAddress(mail));
                    message.Subject = "Masz to co chciałeś";
                    message.Body = imageSRC;
                    SmtpClient smtp = new SmtpClient("smtp.gmail.com");
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential("adresnadawcy", "hasło");
                    smtp.EnableSsl = true;
                    smtp.Port = 587;
                    smtp.Send(message);
                    break;
                }
                else{
                MessageBox.Show("Szukany wyraz nie wystepuje w tekscie.");
                return;
                }
            }
            
 }

      
        private void loadImg(String html)
        {
           
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(html);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            System.IO.Stream receiveStream = response.GetResponseStream();
            System.Drawing.Bitmap image = new Bitmap(receiveStream);
            receiveStream.Close();
            response.Close();
            
        }
    }
}
